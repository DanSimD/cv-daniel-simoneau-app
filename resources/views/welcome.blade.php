<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js"> <!--<![endif]-->
<head>


    <!-- Metas -->
    <meta charset="utf-8">
    <title>CV - DANIEL SIMONEAU D.</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1"/>

    <!-- CSRF Token -->
    {{--// DS: le jeudi 9 juill 2020 à 13:20--}}
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Styles -->
    {{-- // DS: le vendredi 8 mai 2020 à 10:21 --}}
    <link href="{{ asset('css/app'.env('MIX_INDEX_RESS').'.css') }}" rel="stylesheet"/>

    <!-- Css -->
    {{--<link href="{{asset('css/bootstrap.css')}}" rel="stylesheet" type="text/css" media="all"/>--}}
    <link href="{{asset('css/owl-carousel/owl.carousel.css')}}" rel="stylesheet" media="all">
    <link href="{{asset('css/owl-carousel/owl.theme.css')}}" rel="stylesheet" media="all"/>
    <link href="{{asset('css/magnific-popup.css')}}" type="text/css" rel="stylesheet" media="all"/>
    <link href="{{asset('css/font.css')}}" rel="stylesheet" type="text/css" media="all">
    <link href="{{asset('css/fontello.css')}}" rel="stylesheet" type="text/css" media="all">
    <link href="{{asset('css/base.css')}}" rel="stylesheet" type="text/css" media="all"/>
    <link href="{{asset('css/main.css')}}" rel="stylesheet" type="text/css" media="all"/>

    <!-- Add icon library -->
    {{--// DS: le jeudi 9 juill 2020 à 14:10--}}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"/>

    <!--[if gte IE 9]>
    <link rel="stylesheet" type="text/css" href="css/ie9.css"/>
    <![endif]-->

    {{-- Update pour commit initial --}}

</head>


<style>

    .panel.panel-default {
        border-color: transparent;
    }

    .progress-bar {
        line-height: 16px;
        border-radius: inherit;
    }

    .panel-default > .panel-heading {
        color: #333;
        border-color: #ddd;
        background-color: transparent;
    }

    .cv_daniel {
        font-size: smaller;
    }


    /*.block-quote {*/

        /*padding: 62px;*/

    /*}*/

</style>


<body>

<!-- Preloader -->
<div id="loader">
    <!-- Preloader inner -->
    <div id="loaderInner">
        <div class="spinner"></div>
    </div>
    <!-- End preloader inner -->
</div>
<!-- End preloader-->


<!--Wrapper-->
<div id="wrapper" class="margLTop  margLBottom">

    <!--Container-->
    <div class="container" id="app">


        <div class="row ">

            {{-- // DS: le mardi 11 août 2020 à 10:45, pour importer message d'erreur généré par Laravel --}}
            {{-- Adaptation de ce qui est généré automatiquement par Laravel --}}
            @guest

                    <?php
                        $__errorArgs = ['email'];
                        $__bag = $errors->getBag($__errorArgs[1] ?? 'default');

                        if ($__bag->has($__errorArgs[0])) :

                            if (isset($message))
                                { $__messageOriginal = $message; }

                            $message = $__bag->first($__errorArgs[0]);

                        endif;
                    ?>

                <choix-profil message-alert="{{ $message ?? '0' }}"></choix-profil>

            @endguest



            <div class="col-md-3 nopr left-content">

                <!--Header-->
                <header id="header">


                    <!--Main header-->
                    <div class="main-header">

                        <!--Profile image-->
                        <figure class="img-profile">
                            <img src="img/profile.jpg" alt=""/>

                            <figcaption class="name-profile">
                                <span>Daniel Simoneau <sup>&reg;</sup></span>
                            </figcaption>

                        </figure>
                        <!--End profile image-->


                        <!--Main navigation-->
                        <nav id="main-nav" class="main-nav clearfix tabbed">

                            <ul>
                                <li class="active"><a href="#about" class="active"><i class="icon-user"></i>CV</a></li>
                                {{--<li><a  href="#portfolio"><i class="icon-camera"></i>Portfolio</a></li>--}}
                                {{--<li><a  href="#blog"><i class="icon-chat"></i>Blog</a></li>--}}
                                <li><a href="#contact"><i class="icon-phone"></i>Contact</a></li>

                            </ul>

                            <a
                                    class="btn btn-default"
                                    href="{{route('profil')}}"
                                    style="margin: 10px 0px 10px 0px; width: 100%;"

                            >Voir un autre profil</a>

                        </nav>
                        <!--End main navigation-->


                    </div>
                    <!--End main header-->

                    <div class="bottom-header bgWhite ofsTSmall ofsBSmall tCenter">
                        <ul class="social">
                            <li><a href="#"><i class="icon-linkedin"></i></a></li>


                        </ul>

                    </div>


                </header>
                <!--End  header-->


            </div>
            <!--End left content-->


            <!--Right content-->
            <div class="col-md-9 right-content">


                <!--About Tab-->
                <section id="about" class="bgWhite ofsInBottom">

                    <!--About -->
                    <div class="about">


                        <!--Main title who am i -->

                        <div class="main-title">
                            <h1> Qui Suis-Je &nbsp&nbsp&nbsp&nbsp <span
                                        style="color: blue; font-size: smaller;"
                                        class="pull-right">
                                            <a href="https://bitbucket.org/DanSimD/cv-daniel-simoneau-app/commits/"
                                               onclick="glb_enregistre_envemement('Clique consulte MAJ pour connaître commits')"
                                               target="_blank">(En construction, maj: 11 août 2020)
                                            </a>
                                </span>
                            </h1>
                            <div class="divider">
                                <div class="zigzag large clearfix " data-svg-drawing="yes">
                                    <svg xml:space="preserve" viewBox="0 0 69.172 14.975" width="37" height="28" y="0px"
                                         x="0px" xmlns:xlink="http://www.w3.org/1999/xlink"
                                         xmlns="http://www.w3.org/2000/svg" version="1.1">
                                    <path d="M1.357,12.26 10.807,2.81 20.328,12.332
                                        29.781,2.879 39.223,12.321 48.754,2.79 58.286,12.321 67.815,2.793 "
                                          style="stroke-dasharray: 93.9851, 93.9851; stroke-dashoffset: 0;"/>
                                    </svg>
                                </div>
                            </div>
                        </div>

                        <!--End main title who am i -->

                        <!--Content who am i-->
                        <div class="content">

                            <!--Block content-->
                            <div class="block-content margBSmall">

                                <div class=" profile margBSmall">
                                    <h1>Daniel Simoneau D.</h1>
                                <h3>Développeur Web / Maîtrise en ingénierie</h3>
                                </div>

                                <texte-introduction></texte-introduction>

                            </div>
                            <!--End block content-->


                            <!--Block content-->
                            <div class="block-content">

                                <div class="info">

                                    <!--Row-->
                                    <div class="row">
                                        <div class="col-md-6">

                                            <ul class="info-list clearfix">
                                                <li><span class="inf">Nom </span> <span class="value">Daniel Simoneau D.</span></li>
                                                <li><span class="inf">Ville</span> <span class="value">Saint-Augustin-De-Desmaures</span>
                                                </li>
                                            </ul>

                                        </div>


                                        <div class="col-md-6">

                                            <ul class="info-list">
                                                <li><span class="inf">Email</span> <span class="value">tao_daniel_simoneau@icloud.com</span></li>
                                            </ul>

                                        </div>


                                    </div>
                                    <!--End row-->

                                </div>

                            </div>
                            <!--End block content-->


                        </div>
                        <!--End content who am i -->


                        <!--Main title professional skills -->

                        <div class="main-title" style="margin-bottom: 30px;">

                            <h1>
                                Compétences professionnelles
                                <br>
                                <span style="font-size: smaller; font-style: italic; color: lightgrey">(Débutant, intermédiaire, spécialiste, expert)</span>
                            </h1>

                            <div class="divider" style="top: 73px;">
                                <div class="zigzag large clearfix " data-svg-drawing="yes">
                                    <svg xml:space="preserve" viewBox="0 0 69.172 14.975" width="37" height="28" y="0px"
                                         x="0px" xmlns:xlink="http://www.w3.org/1999/xlink"
                                         xmlns="http://www.w3.org/2000/svg" version="1.1">
                                    <path d="M1.357,12.26 10.807,2.81 20.328,12.332
                                        29.781,2.879 39.223,12.321 48.754,2.79 58.286,12.321 67.815,2.793 "
                                          style="stroke-dasharray: 93.9851, 93.9851; stroke-dashoffset: 0;"/>
                                    </svg>
                                </div>


                            </div>


                        </div>

                        <!--End main title professional skills -->

                        <!--Content professional skills -->
                        <div class="content">


                            <progress-bar-informatique></progress-bar-informatique>


                            <progress-bar-enseignement></progress-bar-enseignement>


                            <progress-bar-ingenierie></progress-bar-ingenierie>


                        </div>
                        <!--End content professional skills -->


                        <!--Main title experience-->

                        <div class="main-title">
                            <h1>expériences</h1>
                            <div class="divider">
                                <div class="zigzag large clearfix " data-svg-drawing="yes">
                                    <svg xml:space="preserve" viewBox="0 0 69.172 14.975" width="37" height="28" y="0px"
                                         x="0px" xmlns:xlink="http://www.w3.org/1999/xlink"
                                         xmlns="http://www.w3.org/2000/svg" version="1.1">
	<path d="M1.357,12.26 10.807,2.81 20.328,12.332
		29.781,2.879 39.223,12.321 48.754,2.79 58.286,12.321 67.815,2.793 "
          style="stroke-dasharray: 93.9851, 93.9851; stroke-dashoffset: 0;"/>
	</svg>
                                </div>
                            </div>
                        </div>

                        <!--End main title experience -->

                        <!--Content experience -->
                        <div class="content">

                            <!--Block content-->
                            <div class="block-content ">


                                <!--Timeline-->
                                <div class="timeline experience">


                                    <!--Row-->
                                    <div class="row ">


                                        <div class="col-md-12">


                                            <!--Experience holder-->
                                            <div class="exp-holder margTop">


                                                <!--Experience-->
                                                <div class="exp">
                                                    <div class="hgroup">
                                                        <h4>Concepteur et programmeur web</h4>
                                                        <h6><i class="icon-calendar"></i>2015 - <span
                                                                    class="current">Actuel</span></h6>
                                                    </div>
                                                    <p>Conception et programmation (frontend/backend/backbone) de la plateforme web <a href="https://www.question-action.com" target="_blank">question-action.com</a> permettant
                                                        de mettre en relation les experts, les conseillers et les clients impliqués dans le domaine des finances personnelles</p>
                                                </div>
                                                <!--End experience-->


                                                <!--Experience-->
                                                <div class="exp">
                                                    <div class="hgroup">
                                                        <h4>Concepteur/programmeur d'une application iPhone</h4>
                                                        <h6><i class="icon-calendar"></i>2013 - 1015</h6>
                                                    </div>
                                                    <p>Conception et programmation des applications <a href="http://www.ezformula.phenox-solutions.com/" target="_blank">"EzFormula / EzFormulaPro"</a> disponibles sur le App Store d'Apple.</p>
                                                </div>
                                                <!--End experience-->


                                                <!--Experience-->
                                                <div class="exp">
                                                    <div class="hgroup">
                                                        <h4>Spécialiste en contrôle de procédés</h4>
                                                        <h6><i class="icon-calendar"></i>2006 - 2012</h6>
                                                    </div>
                                                    <p>
                                                        <a href="http://www.cv-danielsimoneau.phenox-solutions.com/Curriculum/Portail_Curriculum/Curriculum_Daniel/Realisations/Controle.html" target="_blank">Études et logiques de contrôle des postes de régulation</a>
                                                        utilisés dans un système de contrôle des débordements.<a href="http://www.cv-danielsimoneau.phenox-solutions.com/Curriculum/Portail_Curriculum/Curriculum_Daniel/PrixTetratec.php" target="_blank">Prix gagné pour ces travaux</a>
                                                    </p>
                                                </div>
                                                <!--End experience-->


                                            </div>
                                            <!--End experience holder-->

                                        </div>


                                    </div>
                                    <!--End row-->

                                </div>
                                <!--End timeline-->

                            </div>
                            <!--End block content-->


                        </div>
                        <!--End content experience -->


                        <!--Main title education-->

                        <div class="main-title">
                            <h1>éducation</h1>
                            <div class="divider">
                                <div class="zigzag large clearfix " data-svg-drawing="yes">
                                    <svg xml:space="preserve" viewBox="0 0 69.172 14.975" width="37" height="28" y="0px"
                                         x="0px" xmlns:xlink="http://www.w3.org/1999/xlink"
                                         xmlns="http://www.w3.org/2000/svg" version="1.1">
	<path d="M1.357,12.26 10.807,2.81 20.328,12.332
		29.781,2.879 39.223,12.321 48.754,2.79 58.286,12.321 67.815,2.793 "
          style="stroke-dasharray: 93.9851, 93.9851; stroke-dashoffset: 0;"/>
	</svg>
                                </div>
                            </div>
                        </div>

                        <!--End main title education-->

                        <!--Content education-->
                        <div class="content">

                            <!--Block content-->
                            <div class="block-content">


                                <!--Timeline-->
                                <div class="timeline education">


                                    <!--Row-->
                                    <div class="row ">


                                        <div class="col-md-12">


                                            <!--Experience holder-->
                                            <div class="exp-holder margTop">

                                                <!--Experience-->
                                                <div class="exp">
                                                    <div class="hgroup">
                                                        <h4>Baccalauréat – Informatique, Université Laval</h4>
                                                        <h6><i class="icon-calendar"></i>2015 - 2018</h6>
                                                    </div>
                                                    <p>
                                                        J'ai entrepris un baccalauréat en informatique que je réalise à distance en dépit de
                                                        toutes mes autres obligations, et ce, afin d'effectuer une réorientation de carrière crédible.
                                                    </p>
                                                </div>
                                                <!--End experience-->

                                                <!--Experience-->
                                                <div class="exp">
                                                    <div class="hgroup">
                                                        <h4>Maîtrise – Génie Chimique, Université Laval</h4>
                                                        <h6><i class="icon-calendar"></i>2004 - 2006</h6>
                                                    </div>
                                                    <p>
                                                        Projet : simulations et stratégies de contrôle applicables à la gestion de la vidange du réservoir
                                                        de rétention du site St-Sacrement du réseau sanitaire de la Ville de Québec.
                                                    </p>
                                                </div>
                                                <!--End experience-->

                                                <!--Experience-->
                                                <div class="exp">
                                                    <div class="hgroup">
                                                        <h4>Baccalauréat – Génie Chimique, Université Laval</h4>
                                                        <h6><i class="icon-calendar"></i>1996 - 1999</h6>
                                                    </div>
                                                    <p>
                                                        Projet de fin d'études : Destruction de l'ozone à l'usine de traitement
                                                        des eaux de la Ville de Sainte-Foy.
                                                    </p>
                                                </div>
                                                <!--End experience-->


                                            </div>
                                            <!--End experience holder-->

                                        </div>


                                    </div>
                                    <!--End row-->

                                </div>
                                <!--End timeline-->

                            </div>
                            <!--End block content-->


                        </div>
                        <!--End content education -->


                        <!--Main title plus à mon sujet-->

                        <div class="main-title">
                            <h1 id="a_mon_sujet">plus à mon sujet</h1>
                            <div class="divider">
                                <div class="zigzag large clearfix " data-svg-drawing="yes">
                                    <svg xml:space="preserve" viewBox="0 0 69.172 14.975" width="37" height="28" y="0px"
                                         x="0px" xmlns:xlink="http://www.w3.org/1999/xlink"
                                         xmlns="http://www.w3.org/2000/svg" version="1.1">
                                    <path d="M1.357,12.26 10.807,2.81 20.328,12.332
                                        29.781,2.879 39.223,12.321 48.754,2.79 58.286,12.321 67.815,2.793 "
                                          style="stroke-dasharray: 93.9851, 93.9851; stroke-dashoffset: 0;"/>
                                    </svg>
                                </div>
                            </div>
                        </div>

                        <!--End main title plus à mon sujet-->

                        <!--Content plus à mon sujet-->
                        <div class="content">

                            <!--Block content-->
                            <div class="block-content">


                                <!--Timeline-->
                                <div class="timeline plus à mon sujet">


                                    <!--Row-->
                                    <div class="row ">


                                        <div class="col-md-12">

                                            <form method="POST" class="form-horizontal" action="{{ route('login') }}">

                                                @csrf

                                                <div class="form-group text-right">

                                                    <div class="col-sm-6 col-xs-2"></div>

                                                    <div class="col-sm-6 col-xs-10">

                                                        <div class="input-group has-error">

                                                            <input id="email"
                                                                   type="email"
                                                                   class="form-control @error('email') is-invalid @enderror"
                                                                   name="email"
                                                                   value="admin@cv-daniel-simoneau.com"
                                                                   required
                                                                   autocomplete="email"
                                                                   autofocus
                                                                   style="display: none"
                                                            >

                                                            @guest

                                                                <input id="password"
                                                                       type="password"
                                                                       class="form-control @error('password') is-invalid @enderror"
                                                                       name="password"
                                                                       required
                                                                       autocomplete="current-password"
                                                                       placeholder="Mot de passe pour afficher la section"
                                                                >

                                                                <span class="input-group-btn">

                                                                <button class="btn btn-primary"
                                                                        type="submit"
                                                                >
                                                                    Consulter
                                                                </button>

                                                            @endguest

                                                                <!--<button class="btn btn-primary"-->
                                                                <!--type="button"-->
                                                                    <!--data-toggle="collapse"-->
                                                                    <!--data-target="#collapseExample"-->
                                                                    <!--aria-expanded="false"-->
                                                                    <!--aria-controls="collapseExample"-->
                                                                    <!--&gt;-->
                                                                    <!--Ouvrir-->
                                                                    <!--</button>-->

                                                        </span>

                                                        </div><!-- /input-group -->

                                                    </div>
                                                </div>

                                            </form>

                                            @auth

                                                <div class="form-group">

                                                    <div class="col-sm-offset-1 col-sm-10 col-xs-offset-2 col-sm-9">

                                                        <div>

                                                            <!-- Nav tabs -->
                                                            <ul class="nav nav-tabs" role="tablist">

                                                                <li role="presentation" class="active"><a
                                                                                            href="#mode_de_vie" aria-controls="mode_de_vie"
                                                                                            role="tab"
                                                                                            data-toggle="tab">Mode de vie</a>
                                                                </li>
                                                                
                                                                <li role="presentation"><a href="#lectures"
                                                                                           aria-controls="lectures"
                                                                                           role="tab"
                                                                                           data-toggle="tab">Lectures</a>
                                                                </li>
                                                                <li role="presentation"><a href="#activites"
                                                                                           aria-controls="activites"
                                                                                           role="tab"
                                                                                           data-toggle="tab">Activités</a>
                                                                </li>
                                                                <li role="presentation"><a href="#convictions"
                                                                                           aria-controls="convictions"
                                                                                           role="tab"
                                                                                           data-toggle="tab">Convictions</a>
                                                                </li>
                                                            </ul>

                                                            <!-- Tab panes -->
                                                            <div class="tab-content">

                                                                <div role="tabpanel" class="tab-pane active"
                                                                     id="mode_de_vie">

                                                                    <div class="panel-body" style="margin-left: 20px;">

                                                                        <ul style="list-style-type: square;">
                                                                            <li>Je suis v&eacute;g&eacute;tarien</li>
                                                                            <li>Je ne bois pas d'alcool</li>
                                                                            <li>Je m&eacute;dite tous les jours
                                                                                <ul style="list-style-type: circle; margin-left: 20px;">
                                                                                    <li>J'essaie d'aller aux fronti&egrave;res de la conscience</li>
                                                                                </ul>
                                                                            </li>

                                                                            <li>&Agrave; chaque jour en me levant, je fais des saluts au soleil</li>
                                                                        </ul>

                                                                    </div>

                                                                </div>


                                                                <div role="tabpanel" class="tab-pane" id="lectures">
                                                                    <br>
                                                                    <p>à venir ...</p>
                                                                </div>


                                                                <div role="tabpanel" class="tab-pane" id="activites">
                                                                    <br>
                                                                    <p>à venir ...</p>
                                                                </div>


                                                                <div role="tabpanel" class="tab-pane" id="convictions">
                                                                    <br>
                                                                    <p>à venir ...</p>
                                                                </div>

                                                            </div>

                                                            <br>

                                                            <a class="dropdown-item" href="{{ route('logout') }}"
                                                               onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                                                {{ __('Logout') }}
                                                            </a>

                                                            <form id="logout-form" action="{{ route('logout') }}"
                                                                  method="POST" style="display: none;">
                                                                @csrf
                                                            </form>

                                                        </div>

                                                    </div>

                                                </div>

                                            @endauth

                                        </div>


                                    </div>
                                    <!--End row-->

                                </div>
                                <!--End timeline-->

                            </div>
                            <!--End block content-->


                        </div>
                        <!--End content plus à mon sujet -->


                        <!--Main title testimonials -->

                        <div class="main-title">
                            <h1>témoignages</h1>
                            <div class="divider">
                                <div class="zigzag large clearfix " data-svg-drawing="yes">
                                    <svg xml:space="preserve" viewBox="0 0 69.172 14.975" width="37" height="28" y="0px"
                                         x="0px" xmlns:xlink="http://www.w3.org/1999/xlink"
                                         xmlns="http://www.w3.org/2000/svg" version="1.1">
	<path d="M1.357,12.26 10.807,2.81 20.328,12.332
		29.781,2.879 39.223,12.321 48.754,2.79 58.286,12.321 67.815,2.793 "
          style="stroke-dasharray: 93.9851, 93.9851; stroke-dashoffset: 0;"/>
	</svg>
                                </div>
                            </div>
                        </div>

                        <!--End main title testimonials -->

                        <!--Content testimonials -->
                        <div class="content">

                            <!--Block content-->
                            <div class="block-content ">


                                <!--Testimonilas-->
                                <div class="testimonials">


                                    <!--Row-->
                                    <div class="row ">

                                        <div class="col-md-12">

                                            <!--Slider-->
                                            <div class="testi clearfix ">


                                                <ul id="testimonial-carousel">

                                                    {{--<li>--}}

                                                        {{--<div class="block-quote">--}}
                                                            {{--<div class="block-img"><img alt=""--}}
                                                                                        {{--src="img/testimonial/1.jpg">--}}
                                                            {{--</div>--}}
                                                            {{--<div class="block-icon"><i class="icon-quote"></i></div>--}}
                                                            {{--<h3 class="block-profile">Carlose Smith <span> CEO / Split Inc</span>--}}
                                                            {{--</h3>--}}
                                                            {{--<blockquote>Sed ut perspiciatis unde omnis iste natus error--}}
                                                                {{--sit voluptatem accusantium doloremque laudantium,--}}
                                                                {{--totam rem aperiam, eaque ipsa quae ab illo--}}
                                                                {{--inventore veritatis et quasi architecto beatae vitae--}}
                                                                {{--dicta.--}}
                                                            {{--</blockquote>--}}

                                                        {{--</div>--}}
                                                    {{--</li>--}}

                                                    <li>

                                                        <div class="block-quote">
                                                            <div class="block-img"><img alt=""
                                                                                        src="img/testimonial/1.jpg">
                                                            </div>
                                                            <div class="block-icon"><i class="icon-quote"></i></div>
                                                            <h3 class="block-profile">Frédéric Kibrité<span> Advisor</span>
                                                            </h3>
                                                            <blockquote>
                                                                Daniel is an exceptional full stack developer capable of transforming an idea into reality
                                                                through a complete user friendly website or application. His engineering background combined
                                                                with his programming skills allow him to quickly seize challenges and propose solutions.
                                                                His curiosity, innovation and ability to use plain language to communicate complexe concepts
                                                                allows him to interface directly with end clients. If your project is engaging and catches his
                                                                attention, he will deliver!
                                                            </blockquote>

                                                        </div>
                                                    </li>

                                                </ul>

                                            </div>
                                            <!--End  slider-->


                                        </div>


                                    </div>
                                    <!--End row-->


                                </div>
                                <!--End testimonials-->

                            </div>
                            <!--End block content-->


                        </div>
                        <!--End content testimonials -->


                        <!--Button-->
                        <div class="button margTop ofsInTop tCenter">

                            <div class="divider">
                                <div class="zigzag large clearfix " data-svg-drawing="yes">
                                    <svg xml:space="preserve" viewBox="0 0 69.172 14.975" width="37" height="28" y="0px"
                                         x="0px" xmlns:xlink="http://www.w3.org/1999/xlink"
                                         xmlns="http://www.w3.org/2000/svg" version="1.1">
		<path d="M1.357,12.26 10.807,2.81 20.328,12.332
			29.781,2.879 39.223,12.321 48.754,2.79 58.286,12.321 67.815,2.793 "
              style="stroke-dasharray: 93.9851, 93.9851; stroke-dashoffset: 0;"/>
		</svg>
                                </div>
                            </div>


                            <!--Row-->
                            <div class="row ">


                                <div class="col-md-12">

                                    <button-download-cv></button-download-cv>

                                </div>


                            </div>
                            <!--End row-->


                        </div>
                        <!--End button-->


                    </div>
                    <!--End about-->

                </section>
                <!--End about tab-->


                <!--Contact Tab-->
                <section id="contact" class="bgWhite ofsInBottom">

                    <!--Contact -->
                    <div class="contact">


                        <!--Main title-->

                        <div class="main-title">
                            <h1>send me an email</h1>
                            <div class="divider">
                                <div class="zigzag large clearfix " data-svg-drawing="yes">
                                    <svg xml:space="preserve" viewBox="0 0 69.172 14.975" width="37" height="28" y="0px"
                                         x="0px" xmlns:xlink="http://www.w3.org/1999/xlink"
                                         xmlns="http://www.w3.org/2000/svg" version="1.1">
				<path d="M1.357,12.26 10.807,2.81 20.328,12.332
					29.781,2.879 39.223,12.321 48.754,2.79 58.286,12.321 67.815,2.793 "
                      style="stroke-dasharray: 93.9851, 93.9851; stroke-dashoffset: 0;"/>
				</svg>
                                </div>
                            </div>
                        </div>

                        <!--End main title-->


                        <!--Content-->
                        <div class="content">

                            <!--Block content-->
                            <div class="block-content ">

                                <!--Contact-->
                                <div class="block-contact margBSSmall">

                                    <!--Row-->
                                    <div class="row">

                                        <div class="col-md-12">

                                            <form-envoi-courriel></form-envoi-courriel>


                                        </div>


                                    </div>
                                    <!--End row-->

                                </div>
                                <!--End contact-->

                            </div>
                            <!--End block content-->


                        </div>
                        <!--End content-->


                        <!--Button-->
                        <div class="button ofsInTop tCenter">

                            <div class="divider">
                                <div class="zigzag large clearfix " data-svg-drawing="yes">
                                    <svg xml:space="preserve" viewBox="0 0 69.172 14.975" width="37" height="28" y="0px"
                                         x="0px" xmlns:xlink="http://www.w3.org/1999/xlink"
                                         xmlns="http://www.w3.org/2000/svg" version="1.1">
			<path d="M1.357,12.26 10.807,2.81 20.328,12.332
				29.781,2.879 39.223,12.321 48.754,2.79 58.286,12.321 67.815,2.793 "
                  style="stroke-dasharray: 93.9851, 93.9851; stroke-dashoffset: 0;"/>
			</svg>
                                </div>
                            </div>


                            <!--Row-->
                            <div class="row ">


                                <div class="col-md-12">

                                    <button-download-cv></button-download-cv>

                                </div>


                            </div>
                            <!--End row-->


                        </div>
                        <!--End button-->


                    </div>
                    <!--End contact-->

                </section>
                <!--End contact tab-->


            </div>
            <!--End right content-->


        </div>
        <!--End  row-->


    </div>
    <!--End  container-->


</div>
<!--End wrapper-->



<!-- Scripts -->
<script src="{{ asset('js/app'.env('MIX_INDEX_RESS').'.js') }}"></script>


<!--Javascript-->
<script src="{{asset('js/jquery-1.11.3.min.js')}}" type="text/javascript"></script>
<script src="{{asset('js/jquery-migrate-1.2.1.js')}}" type="text/javascript"></script>
<script src="{{asset('js/owl.carousel.js')}}" type="text/javascript"></script>
<script src="{{asset('js/jquery.magnific-popup.js')}}" type="text/javascript"></script>
<script src="{{asset('js/jquery.easytabs.min.js')}}" type="text/javascript"></script>
<script src="{{asset('js/jquery.easing.1.3.js')}}" type="text/javascript"></script>
<script src="{{asset('js/modernizr.js')}}" type="text/javascript"></script>
<script src="{{asset('js/placeholders.min.js')}}" type="text/javascript"></script>
<script src="{{asset('js/script.js')}}" type="text/javascript"></script>
{{--<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&amp;sensor=false"></script>--}}


<script>

    (function ($) {

        "use strict";

        $(document).ready(function () {

            $('#wrapper').easytabs({
                animate: true,
                updateHash: false,
                transitionIn: 'fadeIn',
                transitionOut: 'fadeOut',
                animationSpeed: 100,
                tabActiveClass: 'active',
                tabs: ' #main-nav.tabbed > ul > li ',
                transitionInEasing: 'linear',
                transitionOutEasing: 'linear'

            });

        });

    })(jQuery);

</script>



<!-- Google analytics -->
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-172918516-1"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-172918516-1');
</script>
<!-- End google analytics -->


</body>
</html>
